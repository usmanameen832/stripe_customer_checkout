<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\WebhookController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/checkout-session', [StripeController::class , 'checkOutSession'])->name('stripe.checkout.get.session');

Route::middleware('stripe_status')->group(function () {
    Route::get('/payment-success', [StripeController::class , 'paymentSuccess'])->name('payment-success');
});
Route::get('/end-trial',[StripeController::class , 'endTrial'])->name('endTrial');
Route::get('/end-subscription',[StripeController::class , 'endSubs'])->name('endSubs');
Route::get('/failed-payment', [StripeController::class , 'failedPayment'])->name('failed-payment');
Route::get('/billing-portal', [StripeController::class , 'billing'])->name('billing');
Route::get('/subscribe',[StripeController::class, 'subscribe'])->name('subscribe');
Route::get('/trial',[StripeController::class, 'trial'])->name('trial');
Route::post('/stripe/webhook', [WebhookController::class, 'handleWebhook'])->name('handleWebhook');
