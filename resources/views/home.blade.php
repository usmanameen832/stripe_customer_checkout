@extends('layouts.app')

@push('css')
    <style>
        body {
            background: #242d60;
        }
        section {
            background: #ffffff;
            display: flex;
            flex-direction: column;
            width: 100%;
            height: 112px;
            border-radius: 6px;
            justify-content: space-between;
        }
        .product {
            display: flex;
        }
        .description {
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        p {
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 20px;
            letter-spacing: -0.154px;
            color: #242d60;
            height: 100%;
            width: 100%;
            padding: 0 20px;
            display: flex;
            align-items: center;
            justify-content: center;
            box-sizing: border-box;
        }
        img {
            border-radius: 6px;
            margin: 10px;
            width: 54px;
            height: 57px;
        }
        h3,
        h5 {
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 20px;
            letter-spacing: -0.154px;
            color: #242d60;
            margin: 0;
        }
        h5 {
            opacity: 0.5;
        }
        #checkout-button {
            height: 36px;
            background: #556cd6;
            position: relative;
            top: 2px;
            color: white;
            font-size: 14px;
            border: 0;
            font-weight: 500;
            cursor: pointer;
            letter-spacing: 0.6;
            border-radius: 0 0 6px 6px;
            transition: all 0.2s ease;
            box-shadow: 0px 4px 5.5px 0px rgba(0, 0, 0, 0.07);
        }
        #checkout-button:hover {
            opacity: 0.8;
        }
    </style>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session('error_message'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error_message') }}
                        </div>
                    @endif
                    @if(isset($user))
                        @if($user->stripe_status == 'trialing')
                                <h1>{{ __('User Subscribed A Package '. $name .' price: '. $price) }}</h1>
                                <h1>Status : Trial</h1>
                                <h1>Trial End At : {{$trial_ends_at}}</h1>
                                <a href="{{route('endTrial')}}" class="btn btn-sm btn-danger">End Trial Now</a>
                        @else
                                <h1>{{ __('User Subscribed A Package '. $name .' price: '. $price) }}</h1>
                                <h1>Status : Subscribed</h1>
                                <a href="{{route('endSubs')}}" class="btn btn-sm btn-danger">End Subscription</a>
                        @endif
                    @else
                        <h1>{{ __('Manage Subscription') }}</h1>
                        <section>
                            <div class="product">
                                <img
                                    src="https://i.imgur.com/EHyR2nP.png"
                                    alt="The cover of Stubborn Attachments"
                                />
                                <div class="description">
                                    <h3>plane2</h3>
                                    <h5>₹400.00 / month</h5>
                                </div>
                            </div>
                            <div class="flex-column">
                                <button type="button" id="checkout-button">Checkout</button>
                                @if($single_trial->single_trial == 1)
                                    <a href="{{route('trial')}}" class="btn btn-dark">Trial</a>
                                @else
                                    <a>you have already take one trail now please subscribe ... !</a>
                                @endif
                            </div>
                        </section>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        // Create an instance of the Stripe object with your publishable API key
        var stripe = Stripe("pk_test_51IsLfHLLNE7VJKg5CdhwevlAvThpuSHiYWEMcXRuyKO3yslEB8EXGYUjEzNDI3JfYD9EMutDyXhiI684634o0h0q00fR8P2N2r");
        var checkoutButton = document.getElementById("checkout-button");
        const myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append('Authorization', '{{csrf_token()}}');
        checkoutButton.addEventListener("click", function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/checkout-session',
                method:'POST',

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    stripe.redirectToCheckout({ sessionId: response.id });
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                    console.error("Error:", error);
                },
            });
        });
    </script>
@endpush
