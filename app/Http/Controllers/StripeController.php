<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use Laravel\Cashier\Cashier;
use App\Models\SubscriptionItem;

class StripeController extends Controller
{
    public function billing(Request $request){
        return $request->user()->redirectToBillingPortal(route('home'));
    }
    
    public function endTrial(Request $request) {
        try {
            $user = $request->user();
            // dd($user->stripe_id);
            $subs = SubscriptionItem::where('stripe_id','=',$user->stripe_id)->first();
            // dd($subs);
            $stripe = new \Stripe\StripeClient(
              env('STRIPE_SECRET')
            );
            $stripe->subscriptions->cancel(
              $subs->subscription_id,
              []
            );
            Subscription::where('stripe_id','=',$request->user()->stripe_id)->delete();
            SubscriptionItem::where('stripe_id','=',$request->user()->stripe_id)->delete();
            User::where('id','=',$request->user()->id)->update([
               'single_trial' => 0
            ]);
            return redirect()->route('home');   
        } catch(\Exception $e) {
            Subscription::where('stripe_id','=',$request->user()->stripe_id)->delete();
            SubscriptionItem::where('stripe_id','=',$request->user()->stripe_id)->delete();
            return redirect()->route('home');
        }
    }
    
     public function endSubs(Request $request) {
        try {
            $user = $request->user();
            // dd($user->stripe_id);
            $subs = SubscriptionItem::where('stripe_id','=',$user->stripe_id)->first();
            // dd($subs);
            $stripe = new \Stripe\StripeClient(
              env('STRIPE_SECRET')
            );
            $stripe->subscriptions->cancel(
              $subs->subscription_id,
              []
            );
            Subscription::where('stripe_id','=',$request->user()->stripe_id)->delete();
            SubscriptionItem::where('stripe_id','=',$request->user()->stripe_id)->delete();
            return redirect()->route('home');   
        } catch(\Exception $e) {
            // dd($e->getMessage());
            $check = str_contains($e->getMessage(),'No such subscription');
            if($check) {
                Subscription::where('stripe_id','=',$request->user()->stripe_id)->delete();
                SubscriptionItem::where('stripe_id','=',$request->user()->stripe_id)->delete();
            }
            return redirect()->route('home');
        }
    }

    public function trial(Request $request) {
        $user = $request->user();
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        $user = $user->createOrGetStripeCustomer();     
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        $date = \Carbon\Carbon::now()->addDays(1);
        $date = Carbon::parse($date);
        $subscription = \Stripe\Subscription::create([
            'customer' => $user->id,
            'items' => [['price' => 'plan_Jbv6S2y5wVzNAh']],
            'trial_end' => $date->timestamp
        ]);
        $paymentMethod = $stripe->paymentMethods->create([
          'type' => 'card',
          'card' => [
            'number' => '4242424242424242',
            'exp_month' => 12,
            'exp_year' => 2028,
            'cvc' => '123',
          ],
        ]);
        $stripe->paymentMethods->attach(
          $paymentMethod->id,
          ['customer' => $subscription->customer]
        );
        $endTrial = \Carbon\Carbon::parse($subscription->trial_end);
        Subscription::where('user_id', '=', $request->user()->id)->delete();
        Subscription::create([
            'user_id' => $request->user()->id,
            'name' => $request->user()->name,
            'stripe_id' => $subscription->customer,
            'stripe_status' => 'trialing',
            'stripe_price' => $subscription->plan->id,
            'trial_ends_at' => $endTrial,
        ]);
         SubscriptionItem::create([
            'subscription_id' => $subscription->id,
            'stripe_id' => $subscription->customer,
            'stripe_price' => $subscription->plan->amount,
            'stripe_product' => $subscription->plan->product,
            'quantity' => $subscription->quantity
        ]);
        User::where('id','=',$request->user()->id)->update([
           'single_trial' => 0
        ]);
        return redirect()->route('home');
    }

    public function paymentSuccess(Request $request) {
        $user = $request->user();
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        if($user->asStripeCustomer()) {
            $user_subscribe = '';
            foreach ($user->subscriptions as $subs) {
                $user_subscribe = $subs;
            }
            $plan = $stripe->plans->retrieve(
                $user_subscribe->stripe_price,
                []
            );
            $listSubs = $stripe->subscriptions->all();
            $subs = '';
            foreach($listSubs->data as $data) {
                if($data->customer == $user->stripe_id) {
                    $subs =  $data;  
                }
            }
            SubscriptionItem::where('subscription_id','=',$subs->id)->delete();
            $check = SubscriptionItem::where('stripe_id','=',$user->stripe_id)->first();
            if($check == '') {
                SubscriptionItem::create([
                    'subscription_id' => $subs->id,
                    'stripe_id' => $user_subscribe->stripe_id,
                    'stripe_price' => $user_subscribe->stripe_price,
                    'stripe_product' => $plan->product,
                    'quantity' => $user_subscribe->quantity
                ]);
            }
        }
        return view('success');
    }

    public function failedPayment() {
        return view('cancel');
    }

    public function checkOutSession(Request $request){
        $request->user()->createOrGetStripeCustomer();
        $array = [
            'customer' => $request->user()->stripe_id,
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price' => 'plan_Jbv6S2y5wVzNAh',
                'quantity' => 1,
            ]],
            'mode' => 'subscription',
            'success_url' => route('payment-success'),
            'cancel_url' => route('failed-payment'),
        ];
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $session = \Stripe\Checkout\Session::create($array);
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        $plan = $stripe->plans->retrieve(
            'plan_Jbv6S2y5wVzNAh',
            []
        );
        Subscription::where('user_id','=',$request->user()->id)->delete();
        Subscription::create([
            'user_id' => $request->user()->id,
            'name' => $request->user()->name,
            'stripe_id' => $request->user()->stripe_id,
            'stripe_status' => 'in_progress',
            'stripe_price' => 'plan_Jbv6S2y5wVzNAh',
            'quantity' => 1
        ]);
        return collect([
            'status' => true,
            'id' => $session->id
        ]);
    }

    public function subscribe(Request $request){
        $user = $request->user();
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        $stripeCustomer = $user->createOrGetStripeCustomer();
        if($user->asStripeCustomer()) {
            $checkout = $request->user()
            ->newSubscription('default', 'plan_Jbv6S2y5wVzNAh')
            ->checkout([
                'success_url' => route('home'),
                'cancel_url' => route('subscribe')
            ]);
        } else {
            dd('create stripe customer first ... !');
        }
    }
}
