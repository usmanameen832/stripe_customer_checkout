<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use App\Models\Subscription;
use App\Models\SubscriptionItem;
use App\Models\User;
use Illuminate\Support\Carbon;


class WebhookController extends CashierController
{
    /**
     * Handle customer subscription payment created.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleCustomerSubscriptionCreated($payload)
    {
        \Log::info('handleCustomerSubscriptionCreated PayLoad ... !');
        \Log::info('subscription created successfully ... !');
        $data = $payload['data']['object'];
        \Log::info($data['customer']);
        $user = User::where('stripe_id','=',$data['customer'])->first();
        \Log::info($user);
        if($user) {
            if (!$user->subscriptions->contains('stripe_id', $data['id'])) {
                if (isset($data['trial_end'])) {
                        $trialEndsAt = Carbon::createFromTimestamp($data['trial_end']);
                } else {
                    $trialEndsAt = null;
                }

                $firstItem = $data['items']['data'][0];
                $isSinglePrice = count($data['items']['data']) === 1;
                
                Subscription::create([
                    'user_id' =>  $user->id,
                    'name' => $user->name,
                    'stripe_id' => $user->stripe_id,
                    'stripe_status' => $data['status'],
                    'stripe_price' => $isSinglePrice ? $firstItem['price']['id'] : null,
                    'quantity' => $isSinglePrice && isset($firstItem['quantity']) ? $firstItem['quantity'] : null,
                    'trial_ends_at' => $trialEndsAt
                ]);
                
                if(isset($trialEndsAt)) {
                    User::where('id','=',$user->id)->update([
                       'single_trial' => 0
                    ]);
                }

                foreach ($data['items']['data'] as $item) {
                    SubscriptionItem::create([
                        'subscription_id' => $item['subscription'],
                        'stripe_id' => $user->stripe_id,
                        'stripe_product' => $item['price']['product'],
                        'stripe_price' => $firstItem['price']['id'],
                        'quantity' => isset($firstItem['quantity']) ? $firstItem['quantity'] : null,
                    ]);
                }
            }
        }
    }
    
     /**
     * Handle customer subscription update created.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleCustomerSubscriptionUpdated($payload)
    {
        \Log::info('handleCustomerSubscriptionUpdated PayLoad ... !');
        $user = User::where('stripe_id','=',$payload['data']['object']['customer'])->first();
        if ($user) {
            $data = $payload['data']['object'];
                if (
                    isset($data['status']) &&
                    $data['status'] === 'incomplete_expired'
                ) {
                    SubscriptionItem::where('stripe_id','=',$user->stripe_id)->delete();
                    Subscription::where('stripe_id','=',$user->stripe_id)->delete();

                    return;
                }
                $subscription = Subscription::where('stripe_id','=',$user->stripe_id)->first();
                $firstItem = $data['items']['data'][0];
                $isSinglePrice = count($data['items']['data']) === 1;

                // Price...
                $subscription->stripe_price = $isSinglePrice ? $firstItem['price']['id'] : null;

                // Quantity...
                $subscription->quantity = $isSinglePrice && isset($firstItem['quantity']) ? $firstItem['quantity'] : null;

                // Trial ending date...
                if (isset($data['trial_end'])) {
                    $trialEnd = Carbon::createFromTimestamp($data['trial_end']);

                    if (! $subscription->trial_ends_at) {
                        $subscription->trial_ends_at = $trialEnd;
                    }
                }

                // Cancellation date...
                if (isset($data['cancel_at_period_end'])) {
                    if ($data['cancel_at_period_end']) {
                        $subscription->ends_at = $subscription->onTrial()
                            ? $subscription->trial_ends_at
                            : Carbon::createFromTimestamp($data['current_period_end']);
                    } elseif (isset($data['cancel_at'])) {
                        $subscription->ends_at = Carbon::createFromTimestamp($data['cancel_at']);
                    } else {
                        $subscription->ends_at = null;
                    }
                }

                // Status...
                if (isset($data['status'])) {
                    \Log::info($data['status']);
                    $subscription->stripe_status = $data['status'];
                    $subscriptionUser = Subscription::where('stripe_id','=',$user->stripe_id)->first();
                    \Log::info($subscriptionUser->trial_ends_at);
                    \Log::info($subscriptionUser->trial_ends_at == '');
                    if($subscriptionUser->trial_ends_at != '')  {
                        $subs = SubscriptionItem::where('stripe_id','=',$user->stripe_id)->first();
                        $stripe = new \Stripe\StripeClient(
                          env('STRIPE_SECRET')
                        );
                         $stripe->subscriptions->cancel(
                          $subs->subscription_id,
                          []
                        );
                        Subscription::where('stripe_id','=',$data['customer'])->delete();
                        SubscriptionItem::where('stripe_id','=',$data['customer'])->delete();
                    }
                }

                $subscription->save();

                // Update subscription items...
                if (isset($data['items'])) {
                    $prices = [];
                    SubscriptionItem::where('stripe_id','=',$data['customer'])->delete();
                    foreach ($data['items']['data'] as $item) {
                        $prices[] = $item['price']['id'];

                        SubscriptionItem::updateOrCreate([
                            'stripe_id' => $data['customer'],
                        ], [
                            'subscription_id' => $item['subscription'],
                            'stripe_product' => $item['price']['product'],
                            'stripe_price' => $item['price']['id'],
                            'quantity' => $item['quantity'] ?? null,
                        ]);
                    }

                    // Delete items that aren't attached to the subscription anymore...
                    SubscriptionItem::whereNotIn('stripe_price', $prices)->delete();
                }
        }
        \Log::info('handleCustomerSubscriptionUpdated PayLoad ... !');
    }
    
    
    /**
     * Handle customer subscription deleted created.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleCustomerSubscriptionDeleted($payload)
    {
        \Log::info('handleCustomerSubscriptionDeleted PayLoad ... !');
        \Log::info($payload['id']);
        foreach($payload['data'] as $data) { 
              Subscription::where('stripe_id','=',$data['customer'])->delete();
              SubscriptionItem::where('stripe_id','=',$data['customer'])->delete();
        }
        \Log::info('handleCustomerSubscriptionDeleted PayLoad ... !');
    }

    /**
     * Handle invoice payment succeeded.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleInvoicePaymentSucceeded($payload) {
        \Log::info('handleInvoicePaymentSucceeded PayLoad ... !');
        // \Log::info($payload['data']);
        \Log::info('payload ... !');
    }
}
