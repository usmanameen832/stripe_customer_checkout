<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\SubscriptionItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        Subscription::where('stripe_status','=','in_progress')->delete();
        $user = Subscription::where('stripe_id','=',Auth::user()->stripe_id)->first();
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        if(isset($user)) {
            $plan = $stripe->plans->retrieve(
                $user->stripe_price,
                []
            );
            $products = $stripe->products->retrieve(
                $plan->product,
                []
            );
        }
        return view('home',[
            'user' => $user,
            'name' => isset($products->name) ? $products->name : '',
            'price' => isset($plan->amount) ? $plan->amount : '',
            'status' => isset($user->stripe_status) ? $user->stripe_status : '',
            'trial_ends_at' => isset($user->trial_ends_at) ? $user->trial_ends_at : '',
            'single_trial' => Auth::user()
        ]);
    }
}
