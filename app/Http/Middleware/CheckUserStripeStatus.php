<?php

namespace App\Http\Middleware;

use App\Models\Subscription;
use App\Models\SubscriptionItem;
use Closure;
use Illuminate\Http\Request;

class CheckUserStripeStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $stripe = Subscription::where('stripe_id','=',\Auth::user()->stripe_id)->first();
        if(isset($stripe) && $stripe->stripe_status == 'active') {
            return $next($request);
        }
        Subscription::where('stripe_id','=',\Auth::user()->stripe_id)->delete();
        SubscriptionItem::where('stripe_id','=',\Auth::user()->stripe_id)->delete();
        return redirect()->route('home')->with('error_message','Stripe Return Status Unpaid');
    }
}
