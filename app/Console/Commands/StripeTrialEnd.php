<?php

namespace App\Console\Commands;

use App\Models\Subscription;
use App\Models\SubscriptionItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StripeTrialEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:trial_ends';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d h:i:s');
        \Log::info($date);
        $subscription = Subscription::whereDate('trial_ends_at','<',$date)->get();
        \Log::info($subscription);
        if(count($subscription) > 0) {
            foreach($subscription as $subs) {
                $customer = $subs->stripe_id;
                if($subs->stripe_status == 'trialing') {
                    \Log::info('trialing');
                    \Log::info($customer);
                    $subs = SubscriptionItem::where('stripe_id','=',$customer)->first();
                    \Log::info($subs->subscription_id);
                    $stripe = new \Stripe\StripeClient(
                        env('STRIPE_SECRET')
                    );
                    $stripe->subscriptions->cancel(
                        $subs->subscription_id,
                        []
                    );
                    User::where('id','=',$subs->user_id)->update([
                      'single_trial' => 0
                    ]);
                    Subscription::where('stripe_id','=',$customer)->delete();
                    SubscriptionItem::where('stripe_id','=',$customer)->delete();
                    \Log::info('trial end successfully .. ! cron command working fine .. !');
                }   
            }
        }
    }
}
